#!/bin/bash
set -e

get_result () {
    local BRANCH=$1
    echo "Getting results for ${BRANCH}..."

    RESULTS="${PWD}/${BRANCH//[\/]/_}_results.txt"

    if [[ -e $RESULTS ]]; then
        echo "Moving previous results to ${RESULTS}_previous"
        mv "$RESULTS" "${RESULTS}_previous"
    fi

    local TMP="benchmark_tmp"
    local GL="git@gitlab.com:BuildStream"

    mkdir -p $TMP
    pushd $TMP
    # ./tempdir

    local DIR="benchmark_debian"

    if [[ ! -d $DIR ]]; then
      git clone $GL/benchmarking/$DIR.git
    fi

    pushd $DIR
    # ./tempdir/benchmark_debian

    # setup buildstream and venv
    DIR="buildstream"
    if [[ -d $DIR ]]; then
        rm -rf $DIR
    fi

    git clone $GL/$DIR.git
    pushd $DIR
    # ./tempdir/benchmark_debian/buildstream
    git checkout "$BRANCH"
    python3 -m venv benchmarks_venv
    # shellcheck disable=SC1091
    source benchmarks_venv/bin/activate
    pip3 install --upgrade pip
    pip3 install cython
    pip3 install -e .
    pip3 install numpy
    popd
    # ./tempdir/benchmark_debian

    # get the debian test source
    DIR="debian-stretch-bst"
    if [[ ! -d $DIR ]]; then
        git clone --branch jennis/add_all_files $GL/benchmarking/$DIR.git
    fi

    pushd $DIR
    # ./tempdir/benchmark_debian/debian-stretch-bst
    git checkout jennis/add_all_files
    ./prepare_project.sh

    cp ../scripts/benchmark_locally.py .
    echo "$BRANCH" >> "$RESULTS"
    python3 -u benchmark_locally.py | tee "$RESULTS"
    deactivate
    # debian source
    popd
    # ./tempdir/benchmark_debian
    popd
    # ./tmpdir
    popd
    # .
}

if [ $# -eq 0 ]; then
    echo "At least one reference is required."
    exit 1
fi

ref_branch=$1
get_result "$ref_branch"
ref_result=$RESULTS
shift

seen=("$ref_branch")

for branch in "$@"; do
    branch_seen=0
    for tested in "${seen[@]}"; do
        if [[ "$tested" == "$branch" ]]; then
            branch_seen=1
            echo "${branch} was already benchmarked, skipping..."
        fi
    done

    if [[ $branch_seen == 0 ]]; then
        get_result "$branch"
        result=$RESULTS
        output="${ref_branch//[\/]/_}-/${branch//[\/]/_}-comparison.txt"
        echo "Printing comparison file from ${ref_result} and ${result} to ${output}."
        python3 printer.py "$ref_result" "$result" | tee "$output"
        seen+=("$branch")
    fi
done
