import os
import re
import numpy
import click

def get_results(path):
    data = []
    results = []
    with open(path, 'r') as fhandle:
        data = fhandle.readlines()

    job = ''
    jobr = re.compile(r'Benchmarking: bst ((?P<show>show)|--builders (?P<num>\d+))')
    datar = re.compile(r"\d/\d: {'max_memory': (?P<mem>[\d.]+), 'clock_time': (?P<time>[\d.]+)}")

    show_cached = False


    for line in data:
        new_job = False
        jobm = re.match(jobr, line)
        if jobm:
            new_job = True
            if jobm.group('show'):
                if show_cached:
                    job = jobm.group('show') + ' cached'
                else:
                    show_cached = True
                    job = jobm.group('show')
            if jobm.group('num'):
                job = 'build-' + jobm.group('num')

        if new_job:
            result = {'job': job, 'time': [], 'memory': []}
            results.append(result)

        for item in results:
            if item['job'] == job:
                datam = re.search(datar, line)
                if datam:
                    item['memory'].append(int(datam.group('mem')))
                    item['time'].append(float(datam.group('time')))

    for result in results:
        result['time'] = get_times(result['time'])
        result['memory'] = get_mems(result['memory'])
    return results


def get_times(times):
    if times:
        return round(numpy.mean(times), 2)
    return None


def get_mems(mems):
    if mems:
        return round(numpy.mean(mems)/1000, 0)
    return None


def get_display_name(path):
    return os.path.splitext(os.path.basename(path))[0]

@click.command()
@click.argument("reference", required=True)
@click.argument("branch", required=True)
def main(reference, branch):
    reference_results = get_results(reference)
    ref_name = get_display_name(reference)
    branch_results = get_results(branch)
    branch_name = get_display_name(branch)

    for item in reference_results:
        _job = item['job']
        _ref_time = item['time']
        _ref_mem = item['memory']

        print("===={}====".format(_job))
        _branch_time = None
        _branch_mem = None
        for _item in branch_results:
            if _item['job'] == _job:
                _branch_time = _item['time']
                _branch_mem = _item['memory']
                break

        if _branch_time is None or _branch_mem is None:
            raise Exception('missing branch results')

        print("{} time: {}".format(ref_name, str(_ref_time)))
        print("{} time: {}".format(branch_name, str(_branch_time)))

        diff = round(100-((_branch_time/_ref_time)*100), 2)
        if diff < 0:
            diff_sign = '+'
        else:
            diff_sign = '-'
        print("which is a {}{}% difference".format(diff_sign, str(abs(diff))))
        print("{} memory: {}".format(ref_name, str(_ref_mem)))
        print("{} memory: {}".format(branch_name, str(_branch_mem)))
        diff = round(100-((_branch_mem/_ref_mem)*100), 2)
        if diff < 0:
            diff_sign = '+'
        else:
            diff_sign = '-'
        print("which is a {}{}% difference".format(diff_sign, str(abs(diff))))

if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
